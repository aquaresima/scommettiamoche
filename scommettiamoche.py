#!/usr/bin/env python
# -*- coding: utf-8 -*-

from bs4 import BeautifulSoup
import requests
from selenium import webdriver
import numpy as np
import pandas as pd

def addHtml(url):
    """
    Get the HTML from a specific url
    """
    r = requests.get(url)
    content = r.content
    soup = BeautifulSoup(content)
    return soup


def parse_company(url):
    """
    Retrieve data from the company table.
    Once I read the html and wrote this, I hope it won't change
    """
    soup = addHtml(url)
    crp = soup.findAll("tr")
    matches = []
    for i in crp:
        matches.append(i.findAll("td"))
    quote = 0
    for match in matches:
        quota = []
        for field in match:
            quota.append(field.text.encode('utf-8'))
        try:
            quote = np.vstack((quote, quota))
        except:
            quote = np.array(quota)

    return pd.DataFrame(data=quote, index=quote[:, 1], \
             columns=["Id", "Avv", "Tipo", "Date", "incontro", "1",\
                      "x", "2", "1x", "x2", "12", "Under", "Over"])

# Let's eextract the real webpage from "nel tuo sito"


def get_data_urls(url):
    # scrapy with a profile
    firefox_profile = webdriver.FirefoxProfile()
    driver = webdriver.Firefox(firefox_profile=firefox_profile)
    driver.get(url)

    # this depend from website
    elem = driver.find_element_by_tag_name("span")
    soup = BeautifulSoup(elem.get_attribute('innerHTML'))
    real_url = "http:"+soup.find("iframe").attrs['src']

    # try the different data fornitors
    splitted = real_url.split("?")
    urls = []
    for i in [1,4,5,6,9]:
        urls.append(splitted[0]+"?Gestore="+str(i)+"&"+splitted[1])
    return urls


def get_data_tables(urls):
    """
    I have no idea how to manage all these tables,
    we should look at the tables and see what is common between
    companies,
    """
    pandas = []
    for i in urls:
        try:
            pandas.append(parse_company(i))
        except BeautifulSoup.ConnectionError:
            pass
    return pd.concat(pandas, keys=np.arange(len(pandas)))

chicken = url="http://www.neltuosito.it/servizi-web/calcio/quote-scommesse.asp"
## Retrieve SNAI, EUROBET, MATCH POINT...
table_urls = get_data_urls(chicken)
data = get_data_tables(table_urls)
quote = list(data.columns.values)[5:]
